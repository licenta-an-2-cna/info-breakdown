import com.google.api.SystemParameterOrBuilder;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.InfoOuterClass;
import proto.InfoServiceGrpc;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        InfoServiceGrpc.InfoServiceStub infoServiceStub = InfoServiceGrpc.newStub(channel);

        System.out.println("MENU");
        System.out.println("1. Set Info");
        System.out.println("2. QUIT");

        boolean isConnected = true;
        while (isConnected) {
            Scanner input = new Scanner(System.in);
            System.out.println("Choose:");
            int option = input.nextInt();

            switch (option) {
                case 1: {
                    Scanner read = new Scanner(System.in).useDelimiter("\n");
                    System.out.println("Name: ");
                    String name = read.next();

                    System.out.println("CNP: ");
                    String cnp = read.next();

                    infoServiceStub.setInfo(
                            InfoOuterClass.Info.newBuilder()
                                    .setName(name)
                                    .setCnp(cnp).build(),

                            new StreamObserver<InfoOuterClass.InfoResponse>() {
                                @Override
                                public void onNext(InfoOuterClass.InfoResponse value) {
                                    System.out.println(value);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    System.out.println("Error: " + t.getMessage());
                                }

                                @Override
                                public void onCompleted() {

                                }
                            }
                    );
                    break;
                }
                case 2: {
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Unknown command. Please insert a valid command!");
            }
        }
        channel.shutdown();
    }
}
