package services;

import com.google.protobuf.Int32Value;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.InfoOuterClass;
import proto.InfoServiceGrpc;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

public class InfoService extends InfoServiceGrpc.InfoServiceImplBase {

    @Override
    public void setInfo(InfoOuterClass.Info request, StreamObserver<InfoOuterClass.InfoResponse> responseObserver) {
        System.out.println("Received Info:\nName: " + request.getName() + "\nCNP: " + request.getCnp());

        InfoOuterClass.InfoResponse.Builder response = InfoOuterClass.InfoResponse.newBuilder();

        if (request.getName().equals("") || request.getCnp().length() < 7 || request.getCnp().length() > 13) {
            Status status = Status.INVALID_ARGUMENT.withDescription("Invalid info!");
            responseObserver.onError(status.asRuntimeException());
        } else {
            response.setName(request.getName());

            if ((request.getCnp().charAt(0) - '0') % 2 == 0)
                response.setGender("Female");
            else
                response.setGender("Male");

            //Calculates Age
            String date;
            if (Integer.parseInt(request.getCnp().substring(1, 3)) > LocalDate.now().getYear() % 100)
                date = "19" + request.getCnp().substring(1, 3) + "-" + request.getCnp().substring(3, 5) + "-" + request.getCnp().substring(5, 7);
            else
                date = "20" + request.getCnp().substring(1, 3) + "-" + request.getCnp().substring(3, 5) + "-" + request.getCnp().substring(5, 7);

            Period p = Period.between(LocalDate.parse(date), LocalDate.now());
            response.setAge(p.getYears());
        }

        responseObserver.onNext(response.build());
        System.out.println("\nSent Info: " + response + "\n");
        responseObserver.onCompleted();
    }
}
